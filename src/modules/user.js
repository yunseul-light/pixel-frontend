export const SET_USER_NAME = 'user/SET_USER_NAME'

const initialState = {
    username: 'default user name'
}

const user = (state = initialState, action) => {
    switch (action.type) {
        case SET_USER_NAME:
            console.log(action.payload)

            return {
                ...state,
                username: action.payload
            }
        default:
            return state
    }
}

export default user

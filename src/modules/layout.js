export const SET_HORIZONTAL = 'layout/SET_HORIZONTAL'
export const SET_THEME = 'layout/SET_THEME'

/**
 * theme : 'default' | 'black'
 */

const initialState = {
    horizontal: true,
    theme: 'default'
}

const user = (state = initialState, action) => {
    switch (action.type) {
        case SET_HORIZONTAL:
            return {
                ...state,
                horizontal: action.payload
            }
        case SET_THEME:
            return {
                ...state,
                theme: action.payload
            }
        default:
            return state
    }
}

export default user

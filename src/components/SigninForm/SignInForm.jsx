import { strapiLogin } from '../../lib/auth'
import Router from 'next/router'
import { Form, Input, Button, Icon, notification } from 'antd'
import logoIcon from '../../../assets/image/layout/logo.png'
import Cookies from 'js-cookie'
import './SignInForm.scss'

const { Item } = Form

class SignInForm extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            data: {
                email: '',
                password: ''
            },
            loading: false,
            error: ''
        }
    }

    componentDidMount() {
        if (this.props.isAuthenticated) {
            // redirect if you're already logged in
            Router.push('/')
        }
    }

    onChange = (propertyName, event) => {
        const { data } = this.state
        data[propertyName] = event.target.value
        this.setState({ data })
    }

    onSubmit = () => {
        const {
            data: { email, username, password }
        } = this.state
        const { context } = this.props

        this.setState({ loading: true })

        strapiLogin(email, password).then(() =>
            console.log(Cookies.get('user'))
        )
    }

    handleSubmit = e => {
        e.preventDefault()
        this.props.form.validateFields((err, values) => {
            if (!err) {
                console.log('Received values of form: ', values)
                strapiLogin(values.email, values.password)
                    .then(() => console.log(Cookies.get('user')))
                    .catch(e => {
                        notification.open({
                            message: 'Pixel 로그인',
                            description:
                                '아이디/패스워드 정보를 다시 확인해주세요.',
                            icon: (
                                <Icon
                                    type='frown'
                                    style={{ color: '#fa8c16' }}
                                />
                            )
                        })
                    })
            }
        })
    }

    render() {
        const { getFieldDecorator } = this.props.form
        const { media } = this.props
        return (
            <>
                <div className='signin-template'>
                    <img
                        className='signin-logo'
                        src={logoIcon}
                        alt='logoIcon'
                    />
                    <Form
                        onSubmit={this.handleSubmit}
                        className='login-form'
                        style={{ width: '100%' }}
                    >
                        <Item>
                            {getFieldDecorator('email', {
                                rules: [
                                    {
                                        type: 'email',
                                        message: '유효한 이메일을 입력해주세요.'
                                    },
                                    {
                                        required: true,
                                        message: '이메일을 입력해주세요.'
                                    }
                                ]
                            })(
                                <Input
                                    prefix={
                                        <Icon
                                            type='user'
                                            style={{ color: 'rgba(0,0,0,.25)' }}
                                        />
                                    }
                                    placeholder='Email'
                                />
                            )}
                        </Item>
                        <Item>
                            {getFieldDecorator('password', {
                                rules: [
                                    {
                                        required: true,
                                        message: '비밀번호를 입력해주세요.'
                                    }
                                ]
                            })(
                                <Input
                                    prefix={
                                        <Icon
                                            type='lock'
                                            style={{ color: 'rgba(0,0,0,.25)' }}
                                        />
                                    }
                                    type='password'
                                    placeholder='Password'
                                />
                            )}
                        </Item>
                        <div className='signin-state'>로그인 상태 유지</div>
                        <div className='signin-loss-text'>
                            아이디, 비밀번호를 분실하셨나요?
                        </div>
                        <div className='signin-loss-func'>
                            <span>아이디 찾기</span> |{' '}
                            <span>비밀번호 찾기</span>
                        </div>
                        <Item>
                            <button
                                type='primary'
                                htmlType='submit'
                                className='form-button signin'
                            >
                                로그인
                            </button>
                        </Item>
                        <button type='primary' className='form-button signup'>
                            회원가입
                        </button>
                    </Form>
                </div>
            </>
        )
    }
}
export default Form.create({ name: 'normal_login' })(SignInForm)

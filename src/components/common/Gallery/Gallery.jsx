import React from 'react'
import ImageGallery from 'react-image-gallery'
import { gql } from 'apollo-boost'
import { useQuery } from '@apollo/react-hooks'
import './Gallery.scss'
import { backendHost } from '../../../lib/common'

const Gallery = () => {
    const { loading, error, data: { plans } = {} } = useQuery(GET_BANNER_CLASS)

    // DOM 개발 가능한 페이지 (THIS DOM, NOT IMAGE)
    const _renderCustom = ({ item }) => {
        return (
            <div
                className='image-gallery-image'
                style={{
                    width: '100%',
                    height: '420px',
                    border: '1px solid red',
                    padding: '200px 50px',
                    fontSize: '60px',
                    background: '#bec8ff'
                }}
            >
                DOM 개발 가능한 페이지 (THIS DOM, NOT IMAGE)
            </div>
        )
    }

    const galleryItems = [
        {
            original:
                'http://pixel.sc:1337/uploads/859e3c05deaf498aaf45c3c8cea234f7.png'
        }
        // {
        //     renderItem: _renderCustom
        // }
    ]

    const onClickEvent = event => {
        console.log(event)
    }

    if (error) return 'Error Loading Dishes'
    if (loading) return 'Error Loading Dishes'

    if (plans) {
        const result = plans[0].plannedclasses.map(planData => {
            return {
                original: backendHost + planData.class.thumbnail[0].url
            }
        })

        return (
            <div className='gallery-template'>
                <ImageGallery
                    items={[...galleryItems, ...result]}
                    showFullscreenButton={false}
                    useBrowserFullscreen={false}
                    showPlayButton={false}
                    showNav={false}
                    showBullets={true}
                    showThumbnails={false}
                    slideInterval={4000}
                    autoPlay
                    onClick={onClickEvent}
                />
            </div>
        )
    } else {
        return <div></div>
    }
}

const GET_BANNER_CLASS = gql`
    query {
        plans(where: { isBanner: true, isActive: true }) {
            title
            designPattern
            plannedclasses {
                class {
                    title
                    description
                    thumbnail {
                        url
                    }
                }
            }
        }
    }
`

export default Gallery

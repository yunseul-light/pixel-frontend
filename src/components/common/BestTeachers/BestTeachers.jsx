import React from 'react'
import VerticalOwnerCard from '../../card/VerticalOwnerCard'
import SelectTeacherCard from '../../card/SelectTeacherCard'
import PropTypes from 'prop-types'
import './BestTeachers.scss'

const BestTeachers = props => (
    <div className='BestTeachersWrapper'>
        <div className='best-teachers-title'>TEACHER</div>
        <div className='select-teachers-content'>
            <SelectTeacherCard />
            <SelectTeacherCard />
        </div>
        <div className='best-teachers-content'>
            <VerticalOwnerCard />
            <VerticalOwnerCard />
            <VerticalOwnerCard />
            <VerticalOwnerCard />
            <VerticalOwnerCard />
        </div>
    </div>
)

BestTeachers.propTypes = {
    // bla: PropTypes.string,
}

BestTeachers.defaultProps = {
    // bla: 'test',
}

export default BestTeachers

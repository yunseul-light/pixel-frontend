import {
    Player,
    ControlBar,
    VolumeMenuButton,
    ForwardControl,
    ReplayControl,
    PlaybackRateMenuButton,
    TimeDivider,
    CurrentTimeDisplay
} from 'video-react'
import './Video.scss'

// import "../../../assets/video/video.css";

const Video = () => (
    <div className='VideoWrapper'>
        <Player
            autoPlay
            src='https://pixel-videos.s3.ap-northeast-2.amazonaws.com/class/ek1231i/1.mp4'
        >
            <ControlBar>
                <ReplayControl seconds={10} order={2.1} />
                <ReplayControl seconds={5} order={2.2} />
                <ForwardControl seconds={5} order={3.1} />
                <ForwardControl seconds={10} order={3.2} />
                <CurrentTimeDisplay order={4.1} />
                <TimeDivider order={4.2} />
                <PlaybackRateMenuButton
                    rates={[5, 2, 1, 0.5, 0.1]}
                    order={7.1}
                />
                <VolumeMenuButton order={3.3} />
            </ControlBar>
        </Player>
    </div>
)

export default Video

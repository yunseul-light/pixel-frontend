import React from 'react'
import ClassCard from '../../card/ClassCard'
import SelectClassCard from '../../card/SelectClassCard'
import PropTypes from 'prop-types'
import './BestClasses.scss'

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/ade37e367a9f4f6eaca9c2b46685a916.jpg'
        }
    ],
    title: '리비한 햇빛우산 선크림',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

const BestClasses = props => (
    <div className='BestClassesWrapper'>
        <div className='best-classes-title'>CLASS</div>
        <div className='select-classes-content'>
            <SelectClassCard />
            <SelectClassCard />
        </div>
        <div className='best-classes-content'>
            <ClassCard classInfo={garaData} />
            <ClassCard classInfo={garaData} />
            <ClassCard classInfo={garaData} />
        </div>
    </div>
)

BestClasses.propTypes = {
    // bla: PropTypes.string,
}

BestClasses.defaultProps = {
    // bla: 'test',
}

export default BestClasses

import PropTypes from 'prop-types'
import menuIcon from '../../../../assets/image/layout/menu.png'
import userIcon from '../../../../assets/image/layout/userIcon.png'
import Link from 'next/link'
import { menuList } from './MenuList'
import './Menu.scss'

const propTypes = {
	modal: PropTypes.bool.isRequired,
	setModal: PropTypes.func.isRequired
}

const Menu = ({ modal, setModal }) => {
	return (
		<div className="menu-template">
			<div className="header">
				<div className="left" />
				<div className="right">
					<img
						className="menu"
						src={menuIcon}
						alt="user-icon"
						onClick={() => setModal(!modal)}
					/>
				</div>
			</div>
			<div className="body">
				<div className="right">
					<img src={userIcon} alt="userIcon" />
					<div className="nickname">닉네임</div>
					<div className="id">(ID)</div>
				</div>
				<div className="left">
					{menuList.map((menu, index) => (
						<Link href={menu.href} key={index}>
							<div className="menu">{menu.name}</div>
						</Link>
					))}
				</div>
			</div>
		</div>
	)
}

Menu.propTypes = propTypes

export default Menu

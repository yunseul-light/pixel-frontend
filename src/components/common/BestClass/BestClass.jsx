import React from 'react'
import likeIcon from '../../../../assets/image/card/like-icon-white.png'
import pixelIcon from '../../../../assets/image/card/pixel.png'
import PropTypes from 'prop-types'
import './BestClass.scss'

const BestClass = props => (
    <div className='BestClassWrapper'>
        <div
            className='best-class-background'
            style={{
                backgroundImage:
                    'url(https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/1002daaf-e782-434a-b29a-a3f962f210ef.png)'
            }}
        ></div>
        <div className='best-class-cover' />
        <div className='best-class-content'>
            <div className='best-class-content-wrapper'>
                <div className='best-class-content-body'>
                    <div className='best-class-content-header'>
                        <div className='best-class-content-title'>
                            PIXEL BEST CLASS
                        </div>
                        <img
                            className='best-class-content-like'
                            src={likeIcon}
                            alt='likeIcon'
                        />
                    </div>
                    <div className='best-class-content-pixel-discription'>
                        픽셀이 강의를 소개하는 소개글이 표시됩니다. 이런 식으로
                        글을 표시합니다.
                    </div>
                    <div cl assName='best-class-content-title'>
                        컨텐츠 제목
                    </div>
                    <div className='best-class-content-username'>닉네임</div>
                    <div className='best-class-content-discription'>
                        Aliquyam est takimata at eirmod aliquyam duo eos no.
                        Consetetur lorem takimata dolor tempor. Aliquyam
                        sadipscing dolor lorem nonumy justo,.
                    </div>
                    <div className='best-class-content-price'>
                        <img
                            className='best-class-content-pixel'
                            src={pixelIcon}
                            alt='PixelIcon'
                        />
                        <span>50,000P</span>
                    </div>
                </div>
                <div
                    className='best-class-content-image'
                    style={{
                        backgroundImage:
                            'url(https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/1002daaf-e782-434a-b29a-a3f962f210ef.png)'
                    }}
                />
            </div>
        </div>
    </div>
)

BestClass.propTypes = {
    // bla: PropTypes.string,
}

BestClass.defaultProps = {
    // bla: 'test',
}

export default BestClass

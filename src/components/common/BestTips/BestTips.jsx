import React from 'react'
import TipCard from '../../card/TipCard'
import SelectClassCard from '../../card/SelectClassCard'
import PropTypes from 'prop-types'
import './BestTips.scss'

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/58f1c4a448284a00a23957d55d5445b4.jpg'
        }
    ],
    title: '리비한 햇빛우산 선크림',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

const BestTips = props => (
    <div className='BestTipsWrapper'>
        <div className='best-tips-title'>TIP</div>
        <div className='select-tips-content'>
            <SelectClassCard />
            <SelectClassCard />
            <SelectClassCard />
        </div>
        <div className='best-tips-content'>
            <TipCard tipInfo={garaData} />
            <TipCard tipInfo={garaData} />
            <TipCard tipInfo={garaData} />
            <TipCard tipInfo={garaData} />
        </div>
    </div>
)

BestTips.propTypes = {
    // bla: PropTypes.string,
}

BestTips.defaultProps = {
    // bla: 'test',
}

export default BestTips

import Router from 'next/router'
import './ButtomSticky.scss'

import homeWhite from '../../../../assets/image/bottomSticky/home-white.png'
import likeWhite from '../../../../assets/image/bottomSticky/like-white.png'
import searchWhite from '../../../../assets/image/bottomSticky/search-white.png'
import sellWhite from '../../../../assets/image/bottomSticky/sell-white.png'
import userWhite from '../../../../assets/image/bottomSticky/user-white.png'

export default () => (
  <div className='buttom-sticky-template'>
    <img src={homeWhite} alt='homeWhite' onClick={() => Router.push('/')} />
    <img
      src={searchWhite}
      alt='searchWhite'
      onClick={() => Router.push('/search')}
    />
    <img
      src={likeWhite}
      alt='likeWhite'
      onClick={() => Router.push('/mypage')}
    />
    <img src={sellWhite} alt='sellWhite' onClick={() => Router.push('/sell')} />
    <img src={userWhite} alt='userWhite' onClick={() => Router.push('/user')} />
  </div>
)

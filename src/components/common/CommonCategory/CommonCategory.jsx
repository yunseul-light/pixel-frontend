import React from 'react'
import PropTypes from 'prop-types'
import './CommonCategory.scss'

const CommonCategory = props => (
    <div className='CommonCategoryWrapper'>
        <div className='cc-title'>CATEGORY</div>
        <div className='cc-category-cards'>
            <CategoryCard
                content='웹툰'
                url='http://pixel.sc:1337/uploads/b461a039232241d68c5e4d353faec833.png'
            />
            <CategoryCard
                content='CG / 일러스트'
                url='http://pixel.sc:1337/uploads/9840b947b9d046fdb0082656aceee91a.png'
            />

            <CategoryCard
                content='3D'
                url='http://pixel.sc:1337/uploads/459a916b226041bb870bc1c91fe89cf2.png'
            />
        </div>
    </div>
)

const CategoryCard = ({ url, content }) => {
    const styles = {
        backgroundImage: `url(${url})`
    }

    return (
        <div className='cc-category-card' style={styles}>
            {content}
        </div>
    )
}

CommonCategory.propTypes = {
    // bla: PropTypes.string,
}

CommonCategory.defaultProps = {
    // bla: 'test',
}

export default CommonCategory

import React from 'react'
import TipCard from '../../card/TipCard'
import PropTypes from 'prop-types'
import './RelevantTip.scss'

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/ade37e367a9f4f6eaca9c2b46685a916.jpg'
        }
    ],
    title: '리비한 햇빛우산 선크림',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}

const RelevantTip = props => (
    <div className='RelevantTipWrapper'>
        <div className='relevant-title'>인기 많은 연관 TIP</div>
        <div className='relevant-grid'>
            <TipCard tipInfo={garaData} />
            <TipCard tipInfo={garaData} />
            <TipCard tipInfo={garaData} />
            <TipCard tipInfo={garaData} />
        </div>
    </div>
)

RelevantTip.propTypes = {
    // bla: PropTypes.string,
}

RelevantTip.defaultProps = {
    // bla: 'test',
}

export default RelevantTip

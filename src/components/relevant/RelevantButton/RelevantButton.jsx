import React from 'react'
import PropTypes from 'prop-types'
import './RelevantButton.scss'

const RelevantButton = ({ title }) => (
    <div className='RelevantButtonWrapper'>
        <div className='relevant-button-title'>{title}</div>
        <div className='relevant-button-text'>클릭해서 둘러보기</div>
    </div>
)

RelevantButton.propTypes = {
    // bla: PropTypes.string,
}

RelevantButton.defaultProps = {
    // bla: 'test',
}

export default RelevantButton

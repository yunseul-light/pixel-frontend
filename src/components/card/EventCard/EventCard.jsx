import PropTypes from 'prop-types'
import rightIcon from '../../../../assets/image/card/right.png'
import './EventCard.scss'

const propTypes = {
	type: PropTypes.number.isRequired
}

const EventCard = ({ type }) => {
	return (
		<div className="event-card-template">
			{type === 1 && (
				<div className="left">
					<div className="sub-text">누구나 선보일 수 있는 나만의 수업</div>
					<div className="main-text">지금 바로 나만의</div>
					<div className="main-text">강의등록 하러 가기</div>
				</div>
			)}
			{type === 2 && (
				<div className="left">
					<div className="sub-text">픽셀 강사님을 기다립니다.</div>
					<div className="sub-text">강의영상 제작부터 프로모션 혜택까지!</div>
					<div className="main-text">프리미엄 강의 제안 하러가기</div>
				</div>
			)}

			<div className="right">
				<img className="right-icon" src={rightIcon}></img>
			</div>
		</div>
	)
}

EventCard.propTypes = propTypes

export default EventCard

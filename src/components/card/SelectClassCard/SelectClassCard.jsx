import React from 'react'
import Link from 'next/link'
import likeIcon from '../../../../assets/image/card/like-icon-white.png'
import pixelIcon from '../../../../assets/image/card/pixel.png'
import PropTypes from 'prop-types'
import './SelectClassCard.scss'

const SelectClassCard = props => (
    <Link href={`/class/5d2fc876109428469b6c94b0`}>
        <div
            className='SelectClassCardWrapper'
            style={{
                backgroundImage:
                    'url(https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/1002daaf-e782-434a-b29a-a3f962f210ef.png)'
            }}
        >
            <div className='scc-cover'>
                <div className='scc-content'>
                    <div className='scc-content-header'>
                        <div className='scc-content-title'>컨텐츠 제목</div>
                        <img
                            className='scc-content-like'
                            src={likeIcon}
                            alt='LikeIcon'
                        />
                    </div>
                    <div className='scc-content-username'>닉네임</div>
                    <div className='scc-content-discription'>
                        Dolores justo erat est aliquyam dolores et sed voluptua
                        ut, vero justo elitr nonumy stet lorem lorem et.
                        Voluptua no.
                    </div>
                    <hr />

                    <div className='scc-content-price'>
                        <img
                            className='scc-content-pixel'
                            src={pixelIcon}
                            alt='PixelIcon'
                        />
                        <span>50,000P</span>
                    </div>
                </div>
            </div>
        </div>
    </Link>
)

SelectClassCard.propTypes = {
    // bla: PropTypes.string,
}

SelectClassCard.defaultProps = {
    // bla: 'test',
}

export default SelectClassCard

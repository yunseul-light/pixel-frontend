import PropTypes from 'prop-types'
import Link from 'next/link'
import likeIcon from '../../../../assets/image/card/like-icon.png'
import pixelIcon from '../../../../assets/image/card/pixel.png'
import { backendHost } from '../../../lib/common'
import { useSelector } from 'react-redux'
import './TipCard.scss'

const propTypes = {
    classInfo: PropTypes.shape({
        id: PropTypes.string.isRequired,
        thumbnail: PropTypes.arrayOf(
            PropTypes.shape({
                url: PropTypes.string
            })
        ).isRequired,
        title: PropTypes.string.isRequired,
        owner: PropTypes.shape({
            username: PropTypes.string.isRequired
        }).isRequired,
        description: PropTypes.string.isRequired,
        hashtags: PropTypes.arrayOf(
            PropTypes.shape({
                tag: PropTypes.string.isRequired
            })
        ).isRequired
    }).isRequired
}

const TipCard = ({ tipInfo }) => {
    const {
        id,
        thumbnail,
        title,
        owner: { username },
        description,
        hashtags
    } = tipInfo

    const { theme } = useSelector(state => state.layout)

    return (
        <Link href={`/class/${id}`} key={id}>
            <div className={`tip-card-template ${theme}`}>
                <img
                    className='tip-card-thumbnail'
                    src={backendHost + thumbnail[0].url}
                    alt='thumbnail'
                ></img>
                <div className='tip-card-title-template'>
                    <div className='tip-card-title'>{title}</div>
                    <img
                        className='tip-card-like'
                        src={likeIcon}
                        alt='likeIcon'
                    ></img>
                </div>
                <div className='username'>{username}</div>
                <div className='description'>{description}</div>
                <hr />
                <div className='tip-card-price'>
                    <img
                        className='tip-card-pixel'
                        src={pixelIcon}
                        alt='pixelIcon'
                    />
                    <span>50,000P</span>
                </div>
            </div>
        </Link>
    )
}

TipCard.propTypes = propTypes

export default TipCard

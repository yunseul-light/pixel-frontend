import './TeacherCard.scss'
import { backendHost } from '../../../lib/common'
import userIcon from '../../../../assets/image/layout/userIcon.png'
import { mapProps } from 'recompose'

const TeacherCard = () => {
  return (
    <div className='teacher-card-template'>
      <div className='info'>
        <div className='user-thumbnail'>
          <img
            src={`${backendHost}${garaData.profileImage.url}`}
            alt='wefwef'
          />
        </div>
        <div className='text-info'>
          <div className='username'>{garaData.username}</div>
          <div className='sub-title'>커리어 및 경력 사항</div>
          <div className='sub-description'>{garaData.description}</div>
        </div>
        <div className='like'>
          <img src={userIcon} alt='user-icon' />
        </div>
      </div>
      <div className='classes'>
        {garaData.ownedClasses.map((ownedClass, index) => (
          <div className='class-thumbnail'>
            <img
              key={index}
              src={`${backendHost}${ownedClass.thumbnail[0].url}`}
            />
          </div>
        ))}
      </div>
    </div>
  )
}

export default TeacherCard

const garaData = {
  username: 'admin2',
  description: 'oijfewjoifewojifewoiwefewfewiofujweofijweofiewj',
  profileImage: {
    url: '/uploads/c0f18fe7f4694acfa3f97df7b8876542.PNG'
  },
  ownedClasses: [
    {
      thumbnail: [
        {
          url: '/uploads/7b156e0f1ca546ab941dab73438b823e.PNG'
        }
      ]
    },
    {
      thumbnail: [
        {
          url: '/uploads/69e67aafdeb94f88a76688f3c005d8e5.PNG'
        }
      ]
    },
    {
      thumbnail: [
        {
          url: '/uploads/50458a95f81d491b99ae55e832ce1c45.PNG'
        }
      ]
    },
    {
      thumbnail: [
        {
          url: '/uploads/dfe06074bb76431bbb0dd97c431d3cc3.PNG'
        }
      ]
    }
  ]
}

import PropTypes from 'prop-types'
import './ClassDetailCard.scss'
import favoriteIcon from '../../../../assets/image/card/favorites.png'

const propTypes = {
    title: PropTypes.string.isRequired,
    lookup: PropTypes.number.isRequired,
    description: PropTypes.string.isRequired,
    hashtags: PropTypes.arrayOf(
        PropTypes.shape({
            tag: PropTypes.string.isRequired
        }).isRequired
    )
}

const ClassDetailCard = ({ title, lookup, description, hashtags }) => {
    return (
        <div className="class-detail-card-template">
            <div className="left">
                <div className="header">
                    <div className="title">{title}</div>
                    <div className="lookup">조회수 {lookup}건</div>
                    <div className="like"> 1건</div>
                </div>
                <div className="description">{description}</div>
                <div className="hashtags">
                    {hashtags.map(hashtag => (
                        <div>#{hashtag.tag}</div>
                    ))}
                </div>
            </div>
            <div className="right">
                <img src={favoriteIcon} />
            </div>
        </div>
    )
}

ClassDetailCard.propTypes = propTypes

export default ClassDetailCard

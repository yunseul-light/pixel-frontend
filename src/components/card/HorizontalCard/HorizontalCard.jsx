import './HorizontalCard.scss'
import Link from 'next/link'
import { backendHost } from '../../../lib/common'
import rightIcon from '../../../../assets/image/card/right.png'

const garaData = {
	id: '5d2fc876109428469b6c94b0',
	thumbnail: [
		{
			url: '/uploads/6b0035e68b4a4cda9075bda1dc4f19fd.PNG'
		}
	],
	title: '리비한 햇빛우산 선크림',
	owner: {
		username: '김재근'
	},
	description: '리비한 햇빛우산 선크림'
}

const HorizontalCard = () => {
	const { id, thumbnail, title, owner, description } = garaData

	return (
		<Link href={`/class/${id}`} key={id}>
			<div className="HorizontalCard">
				<div className="left">
					<img src={backendHost + thumbnail[0].url} alt="card img" />
				</div>
				<div className="right">
					<div className="title">{title}</div>
					<div className="username">{owner.username}</div>
					<div className="description">{description}</div>
				</div>
				<div className="right-icon">
					<div>
						<img src={rightIcon} alt="rightIcon" />
					</div>
				</div>
			</div>
		</Link>
	)
}

export default HorizontalCard

import PropTypes from 'prop-types'
import Link from 'next/link'
import likeIcon from '../../../../assets/image/card/like-icon.png'
import pixelIcon from '../../../../assets/image/card/pixel.png'
import { backendHost } from '../../../lib/common'
import './SimpleClassCard.scss'

const propTypes = {
    classInfo: PropTypes.shape({
        id: PropTypes.string.isRequired,
        thumbnail: PropTypes.arrayOf(
            PropTypes.shape({
                url: PropTypes.string
            })
        ).isRequired,
        title: PropTypes.string.isRequired
    }).isRequired
}

const SimpleClassCard = ({ classInfo }) => {
    const {
        id,
        thumbnail,
        title
        // owner: { username },
        // description,
        // hashtags
    } = classInfo

    return (
        <Link href={`/class/${id}`} key={id}>
            <div className='simple-class-card-template'>
                <img
                    className='thumbnail'
                    src={backendHost + thumbnail[0].url}
                    alt='thumbnail'
                ></img>
                <div className='simple-class-card-content'>
                    <div className='title-template'>
                        <div className='text'>
                            <div className='title'>{title}</div>
                        </div>
                        <div className='img'>
                            <img src={likeIcon} alt='likeIcon'></img>
                        </div>
                    </div>
                    <div className='simple-class-username'>닉네임</div>
                    <div className='simple-class-description'>
                        Sed est diam ipsum diam dolore elitr. Sit clita sea
                        gubergren sed labore. Rebum labore erat vero eirmod
                        eirmod takimata.
                    </div>
                    <hr />
                    <div className='simple-class-price'>
                        <img
                            className='simple-class-pixel'
                            src={pixelIcon}
                            alt='PixelIcon'
                        />
                        <span>50,000P</span>
                    </div>
                </div>
            </div>
        </Link>
    )
}

SimpleClassCard.propTypes = propTypes

export default SimpleClassCard

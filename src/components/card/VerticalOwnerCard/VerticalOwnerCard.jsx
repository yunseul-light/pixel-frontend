import React from 'react'
import PropTypes from 'prop-types'
import { backendHost } from '../../../lib/common'
import classIcon from '../../../../assets/image/card/class-icon.png'
import tipIcon from '../../../../assets/image/card/tip-icon.png'
import userIcon from '../../../../assets/image/card/user-icon.png'
import './VerticalOwnerCard.scss'

const VerticalOwnerCard = props => (
    <div className='VerticalOwnerCardWrapper'>
        <img
            className='user-image'
            src={`${backendHost}/uploads/021a12d483674a27bdc4d536a29a1047.jpg`}
        ></img>
        <div className='username'>닉네임</div>
        <img className='follow-icon' src={userIcon} alt='userIcon' />
        <div className='follow-count'>347</div>
        <div className='career'>경력 사항1</div>
        <div className='career'>경력 사항2</div>
        <div className='discription'>
            Magna dolor justo et rebum rebum vero lorem rebum, at dolor accusam
            eos et. Amet lorem tempor sit vero est elitr sed. Amet sed invidunt
            amet dolor ipsum ut rebum tempor invidunt,
        </div>
        <hr />
        <div className='class-and-tip'>
            <img src={classIcon} />
            <span>4</span>
            <img src={tipIcon} />
            <span>34</span>
        </div>
    </div>
)

VerticalOwnerCard.propTypes = {
    // bla: PropTypes.string,
}

VerticalOwnerCard.defaultProps = {
    // bla: 'test',
}

export default VerticalOwnerCard

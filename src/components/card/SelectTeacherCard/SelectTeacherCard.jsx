import React from 'react'
import Link from 'next/link'
import classIcon from '../../../../assets/image/card/class-icon-white.png'
import tipIcon from '../../../../assets/image/card/tip-icon-white.png'
import textLogoIcon from '../../../../assets/image/layout/text-logo-white.png'
import PropTypes from 'prop-types'
import './SelectTeacherCard.scss'

const SelectTeacherCard = props => (
    <Link href={`/mypage/5d2fc876109428469b6c94b0`}>
        <div className='SelectTeacherCardWrapper'>
            <div
                className='stc-cover'
                style={{
                    backgroundImage:
                        'url(https://images.unsplash.com/photo-1570737044764-efc7cc50cf2c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2134&q=80)'
                }}
            />
            <div className='stc-cover-black' />
            <div className='stc-content'>
                <div className='stc-info'>
                    <img
                        src='https://images.unsplash.com/photo-1570737044764-efc7cc50cf2c?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2134&q=80'
                        alt='teacherThumbnail'
                    />
                    <div className='stc-info-text'>
                        <div className='stc-teacher-username'>닉네임</div>
                        <div className='stc-teacher-career'>
                            <div className='stc-career'>경력 사항1</div>
                            <div className='stc-career'>경력 사항2</div>
                        </div>

                        <div className='stc-teacher-discription'>
                            Ipsum sed sit gubergren diam rebum et labore,
                            sadipscing sanctus sed sed clita labore ipsum amet,
                            lorem consetetur lorem no et dolore magna ut elitr
                            erat, clita est lorem aliquyam.
                        </div>
                    </div>
                </div>
                <div className='stc-class-tip'>
                    <div className='stc-class'>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                    </div>
                    <div className='stc-tip'>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                        <img src='https://cdn.zeplin.io/5d6fc79f4dbe0918d50c8354/assets/e7a691c2-acfd-4913-a62e-397f38217e00.png'></img>
                        <div className='stc-more'>더보기</div>
                    </div>
                </div>
                <hr />
                <div className='stc-footer'>
                    <div className='stc-cnt'>
                        <img src={classIcon} />
                        <span>4</span>
                        <img src={tipIcon} />
                        <span>34</span>
                    </div>
                    <div className='stc-select-text'>PIXEL 선정</div>
                </div>
                <div className='stc-follow'></div>
            </div>
        </div>
    </Link>
)

SelectTeacherCard.propTypes = {
    // bla: PropTypes.string,
}

SelectTeacherCard.defaultProps = {
    // bla: 'test',
}

export default SelectTeacherCard

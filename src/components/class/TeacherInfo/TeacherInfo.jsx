import React from 'react'
import pixelIcon from '../../../../assets/image/card/pixel.png'
import { Scrollbars } from 'react-custom-scrollbars'
import PropTypes from 'prop-types'
import './TeacherInfo.scss'

const TeacherInfo = props => (
    <div className='TeacherInfoWrapper'>
        <div className='teacher-info-title-wrapper'>
            <div>컨텐츠 제목</div>
            <img />
        </div>
        <div className='teacher-info-wrapper2'>
            <div className='teacher-info discription'>
                <div className='teacher-discription'>
                    <Scrollbars>
                        포토샵 브러쉬 만드는 노하우 대공개 효과 브러쉬 포함!!
                        사용법 영상 강의를 확인하세요. 상세 설명에 자세한 내용을
                        확인하세요!
                    </Scrollbars>
                </div>
                <div className='teacher-category'>웹툰 - 채색</div>
            </div>
            <div className='teacher-info price'>
                <img src={pixelIcon} alt='pixelIcon' /> 50,000P
            </div>
        </div>
        <div className='teacher-info-wrapper3'>
            <div className='teacher-info part'>
                <div id='item-title'>강의 파트</div>
                <div id='teacher-info-scrollbars'>
                    <Scrollbars>
                        <div id='scrollbars-item'>
                            <div>강의 파트1</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트2</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트3</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트4</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트5</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트6</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트7</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트8</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트9</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트10</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트11</div>
                            <div>00:00</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>강의 파트12</div>
                            <div>00:00</div>
                        </div>
                    </Scrollbars>
                </div>
            </div>
            <div className='teacher-info data'>
                <div id='item-title'>CLASS 자료</div>
                <div className='teacher-info-data' id='teacher-info-scrollbars'>
                    <Scrollbars>
                        <div id='scrollbars-item'>
                            <div>CALSS 자료1</div>
                            <div>Web Video</div>
                        </div>
                        <div id='scrollbars-item'>
                            <div>CALSS 자료</div>
                            <div>PSD</div>
                        </div>
                    </Scrollbars>
                </div>
                <button className='class-data-download'>
                    CLASS 자료 다운로드
                </button>
            </div>
            <div className='teacher-info user'>3</div>
        </div>
    </div>
)

TeacherInfo.propTypes = {
    // bla: PropTypes.string,
}

TeacherInfo.defaultProps = {
    // bla: 'test',
}

export default TeacherInfo

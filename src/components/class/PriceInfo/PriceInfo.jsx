import PropTypes from 'prop-types'
import { numberWithCommas } from '../../../lib/common'
import './PriceInfo.scss'

const proptTypes = {
    price: PropTypes.number.isRequired,
    discount: PropTypes.number.isRequired
}

const PriceInfo = ({ price, discountPrice }) => {
    return (
        <div className="price-info-template">
            <div className="body">
                <div className="discount">
                    {Math.round(((price - discountPrice) / price) * 100)}%할인
                </div>
                <div className="price">{numberWithCommas(discountPrice)}원</div>
                <div className="detail-price">
                    <span>{numberWithCommas(price)}원</span> -> {numberWithCommas(discountPrice)}원
                </div>
                <div className="process">0개월 과정</div>
            </div>
        </div>
    )
}

PriceInfo.proptTypes = proptTypes

export default PriceInfo

export const numberWithCommas = number => {
    return number.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',')
}

export const backendHost = process.env.BACKEND_DOMAIN || 'http://localhost:1337'

export const secondsToTime = seconds => {
    const temp = seconds % 60
    const secondsDigit = temp < 10 ? `0${temp}` : `${temp}`

    return `${Math.floor(seconds / 60)}:${secondsDigit}`
}

module.exports = {
  apps: [
    {
      name: 'Pixel Frontend',
      cwd: '/usr/local/pixel-frontend',
      script: 'yarn',
      args: 'start',
      env: {
        NODE_ENV: 'production',
	PORT: 80,
	BACKEND_DOMAIN: 'http://pixel.sc:1337'
      },
    },
  ],
};

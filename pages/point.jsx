import pointImage from '../assets/image/point/point.jpg'
import '../src/styles/pages/point.scss'

const Point = () => {
    return (
        <div className='pointWrapper'>
            <div className='point-page-title'>TIP POINT</div>
            <img src={pointImage} alt='pointImage' />
            <div className='point-page-grid'>
                <div className='point-page-content'>
                    <div className='point-page-content1'>
                        팁 포인트가 준비되었습니다.
                    </div>
                    <div className='point-page-content2'>
                        팁 포인트를 지금 수령하세요!
                    </div>
                    <div className='point-page-content3'>
                        Duo nonumy sed eirmod accusam ipsum nonumy diam et,
                        invidunt no lorem sit et elitr tempor nonumy ut eirmod,
                        tempor erat rebum dolore sadipscing ea kasd elitr ut,
                        invidunt no kasd ut rebum rebum diam sed nonumy. Sea sea
                        takimata.
                    </div>
                </div>
                <div className='point-page-btn'>
                    <button>팁 포인트 받기</button>
                </div>
            </div>
        </div>
    )
}

export default Point

import { useRouter } from 'next/router'
import { useQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'
import Link from 'next/link'
import { backendHost } from '../src/lib/common'
import { showPaymentPage } from '../src/lib/payment'
import ClassDetailCard from '../src/components/card/ClassDetailCard'
import '../src/styles/pages/payment.scss'

const GET_CLASS = gql`
    query($id: ID!) {
        class(id: $id) {
            id
            title
            description
            lookup
            price
            discountPrice
            owner {
                username
                profileImage {
                    url
                }
            }
            isActive
            thumbnail {
                url
            }
            detailImages {
                url
            }
            hashtags {
                tag
            }
            free
        }
    }
`

const Payment = () => {
    const router = useRouter()
    const paymentId = router.query.id

    const { loading, error, data } = useQuery(GET_CLASS, {
        variables: { id: paymentId }
    })

    if (loading) return <div>Loading</div>
    if (error) return <div>{`Error! ${error}`}</div>

    if (data) {
        const {
            id,
            title,
            description,
            lookup,
            owner,
            thumbnail,
            detailImages,
            hashtags,
            price,
            discountPrice
        } = data.class

        return (
            <div>
                <img src={backendHost + thumbnail[0].url} alt="thumbnail" />
                <ClassDetailCard
                    title={title}
                    lookup={lookup}
                    description={description}
                    hashtags={hashtags}
                />
                <div className="margin-bt" />

                <div
                    className="payment-btn"
                    onClick={() => {
                        showPaymentPage()
                    }}
                >
                    결제하기
                </div>
            </div>
        )
    }
}

export default Payment

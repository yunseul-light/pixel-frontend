import React from 'react'
import App from 'next/app'
import Router from 'next/router'
import { withApollo } from '../src/lib/apollo'
import Layout from '../src/components/Layout'
import rootReducer from '../src/modules'
import { createStore } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import { Provider } from 'react-redux'
import * as gtag from '../src/lib/gtag'

const store = createStore(rootReducer, composeWithDevTools())
Router.events.on('routeChangeComplete', url => gtag.pageview(url))
class MyApp extends App {
	static async getInitialProps({ Component, router, ctx }) {
		let pageProps = {}

		if (Component.getInitialProps) {
			pageProps = await Component.getInitialProps(ctx)
		}
		return { pageProps }
	}

	render() {
		const { Component, pageProps } = this.props

		return (
			<>
				<script
					src="https://cdn.bootpay.co.kr/js/bootpay-3.0.2.min.js"
					type="application/javascript"
				></script>
				<Provider store={store}>
					<Layout>
						<Component {...pageProps} />
					</Layout>
				</Provider>
			</>
		)
	}
}

export default withApollo(MyApp)

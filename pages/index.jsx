import React from 'react'
import Gallery from '../src/components/common/Gallery'
import BestTeachers from '../src/components/common/BestTeachers'
import BestClasses from '../src/components/common/BestClasses'
import BestClass from '../src/components/common/BestClass'
import BestTips from '../src/components/common/BestTips'
import CommonCategory from '../src/components/common/CommonCategory'
import NewClassesCard from '../src/components/card/NewClassesCard'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { SET_THEME, SET_HORIZONTAL } from '../src/modules/layout'
import '../src/styles/pages/index.scss'

const Index = () => {
    /** Create Dispatcher */
    const dispatcher = useDispatch()
    const setDefaultTheme = () => {
        dispatcher({ type: SET_THEME, payload: 'default' })
    }
    const setHorizontal = () => {
        dispatcher({ type: SET_HORIZONTAL, payload: true })
    }

    useEffect(() => {
        setDefaultTheme()
        setHorizontal()
    }, [])

    return (
        <div className='main-page-wrapper'>
            <Gallery />
            <Padding>
                <BestClasses />
            </Padding>
            <Padding>
                <TempEventCard />
            </Padding>
            <Padding>
                <BestTips />
            </Padding>
            <BestClass />
            <Padding>
                <TempEventCard />
            </Padding>
            <Padding>
                <BestTeachers />
            </Padding>
            <Padding>
                <TempEventCard />
            </Padding>
            <Padding>
                <CommonCategory />
            </Padding>
            <Padding>
                <NewClassesCard />
            </Padding>
            {/* <PlanList /> */}
        </div>
    )
}

const Padding = ({ children }) => (
    <div className='padding-wrapper'>{children}</div>
)

const TempEventCard = () => (
    <div className='temp-event-card'>PIXEL EVENT CARD</div>
)

export default Index

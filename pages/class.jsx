import { useRouter } from 'next/router'
import { useQuery } from '@apollo/react-hooks'
import { gql } from 'apollo-boost'
import PlanCard from '../src/components/card/PlanCard'
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { SET_THEME, SET_HORIZONTAL } from '../src/modules/layout'
import ClassInfo from '../src/components/class/ClassInfo'
import Video from '../src/components/common/Video'
import RelevantClass from '../src/components/relevant/RelevantClass'
import RelevantTip from '../src/components/relevant/RelevantTip'
import RelevantButton from '../src/components/relevant/RelevantButton'
import '../src/styles/pages/class.scss'

const GET_CLASS = gql`
    query($id: ID!) {
        class(id: $id) {
            id
            title
            description
            lookup
            price
            discountPrice
            owner {
                username
                profileImage {
                    url
                }
            }
            isActive
            thumbnail {
                url
            }
            detailImages {
                url
            }
            hashtags {
                tag
            }
            free
        }
    }
`

const ClassDetail = () => {
    const router = useRouter()
    const { loading, error, data } = useQuery(GET_CLASS, {
        variables: { id: router.query.id }
    })

    /** Create Dispatcher */
    const dispatcher = useDispatch()
    const setDefaultTheme = () => {
        dispatcher({ type: SET_THEME, payload: 'black' })
    }
    const setHorizontal = () => {
        dispatcher({ type: SET_HORIZONTAL, payload: false })
    }

    useEffect(() => {
        setDefaultTheme()
        setHorizontal()
    }, [])

    if (loading) return <div>Loading</div>
    if (error) return <div>{`Error! ${error}`}</div>

    if (data) {
        const {
            id,
            title,
            description,
            lookup,
            owner,
            thumbnail,
            detailImages,
            hashtags,
            price,
            discountPrice
        } = data.class

        return (
            <div className='classWrapper'>
                <div className='class-padding'>
                    <div>
                        <Video />
                        <ClassInfo />
                        <div className='class-detail-content'>
                            <div className='class-detail-content-title'>
                                상세설명
                            </div>
                            <div className='class-detail-content-body'>
                                Ea eos lorem ut kasd invidunt elitr vero elitr,
                                ut eirmod et accusam sed lorem ut ipsum dolor,
                                aliquyam labore stet invidunt gubergren stet,
                                amet diam rebum dolor voluptua sanctus amet ut
                                sed, diam ipsum magna accusam rebum dolores
                                accusam diam. Et rebum eirmod sed invidunt ut
                                sed. Duo tempor ea sadipscing no, magna dolor
                                sed stet sit diam sea ipsum ea. Justo amet
                                labore voluptua dolor dolore dolore invidunt
                                erat tempor, lorem et duo et invidunt ea,
                                sanctus magna ipsum amet et dolor et ut,
                                aliquyam stet lorem voluptua et, justo eos
                                gubergren ea takimata. Tempor voluptua no tempor
                                magna sit no dolor dolore. Takimata ut erat
                                takimata kasd takimata eos duo stet est,
                                aliquyam stet lorem vero nonumy et duo est. Erat
                                et sed ipsum magna nonumy, et dolores ipsum
                                sanctus amet rebum. Diam invidunt dolore justo
                                ut ipsum lorem sanctus. Eirmod justo sea sit ut.
                                Ipsum eirmod diam ut et clita stet. Diam vero
                                erat dolor stet erat tempor. Sea invidunt kasd
                                elitr voluptua, et invidunt eirmod dolor
                                invidunt sed vero sed. Duo takimata est elitr et
                                et erat rebum lorem sed. Dolore no dolor vero
                                invidunt sea sed nonumy vero et. Sit diam
                                sadipscing ipsum clita magna rebum. Dolore no
                                lorem sanctus elitr sit sea amet ipsum ea. Sit
                                nonumy et dolore ut no diam dolore, est diam
                                dolor sit consetetur sadipscing aliquyam lorem
                                et. Justo diam et sed dolores et, labore
                                aliquyam takimata justo clita. Dolor tempor amet
                                sanctus elitr takimata. Magna dolor gubergren
                                gubergren sed gubergren. Est ut amet ea ea ea,
                                gubergren lorem stet et dolor, labore labore
                                vero vero sit ipsum, voluptua ipsum sadipscing
                                no gubergren et kasd diam. Diam ut ipsum kasd
                                stet sadipscing, dolore stet aliquyam dolor
                                consetetur at et et, elitr vero nonumy lorem
                                erat accusam diam eos, tempor sed elitr et
                                aliquyam nonumy. Dolor amet stet labore tempor
                                justo stet invidunt stet sea, takimata voluptua
                                aliquyam sit est diam. Amet dolor gubergren
                                voluptua kasd eirmod ut, ipsum magna rebum
                                dolores lorem invidunt accusam ut amet sed, diam
                                labore takimata clita dolor sed stet, dolor
                                ipsum erat diam et elitr labore duo, lorem
                                tempor et sit elitr ut stet eos gubergren
                                takimata, at sadipscing vero consetetur
                                voluptua. Diam et gubergren et amet takimata
                                tempor lorem at. Erat sadipscing vero accusam
                                eirmod. Nonumy ea sed et amet erat sea elitr,
                                erat sed eos ipsum accusam sit clita sit erat,
                                labore.
                            </div>
                        </div>
                    </div>

                    <RelevantClass />
                    <RelevantTip />
                    <div className='class-relevant-buttons'>
                        <RelevantButton title='이 판매자의 다른 CLASS와 TIP을 둘러보시겠어요?' />
                        <RelevantButton title='연관된 CLASS와 TIP을 둘러보시겠어요?' />
                    </div>
                </div>
            </div>
        )
    }

    return <div>class detail</div>
}

export default ClassDetail

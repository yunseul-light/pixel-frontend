import { useState } from 'react'
import { Row, Col, Menu } from 'antd'
import SearchInput from '../src/components/common/SearchInput'
import HorizontalCard from '../src/components/card/HorizontalCard'
import ClassCard from '../src/components/card/ClassCard'
import '../src/styles/pages/mypage.scss'

const menuItemStyles = {
    width: '50%',
    textAlign: 'center'
}

const garaData = {
    id: '5d2fc876109428469b6c94b0',
    thumbnail: [
        {
            url: '/uploads/6b0035e68b4a4cda9075bda1dc4f19fd.PNG'
        }
    ],
    title: '리비한 햇빛우산 선크림',
    owner: {
        username: '김재근'
    },
    description: '리비한 햇빛우산 선크림',
    hashtags: [
        {
            tag: '해시1'
        },
        {
            tag: '영상 강의'
        },
        {
            tag: 'E북 강의'
        }
    ]
}
const mypage = () => {
    const [key, setKey] = useState(['default'])

    const handleClick = e => {
        console.log('click ', e)
        setKey([e.key])
    }

    return (
        <div>
            <SearchInput />
            {typeof window !== 'undefined' && (
                <Menu mode="horizontal" defaultSelectedKeys={key} onClick={handleClick}>
                    <Menu.Item key="default" style={menuItemStyles}>
                        나의 목록
                    </Menu.Item>
                    <Menu.Item key="like" style={menuItemStyles}>
                        찜한 목록
                    </Menu.Item>
                </Menu>
            )}

            {key[0] === 'default' && (
                <div>
                    <SubHeader title="알림" />
                    <HorizontalCard /> <HorizontalCard />
                    <HorizontalCard /> <HorizontalCard /> <HorizontalCard /> <HorizontalCard />{' '}
                    <HorizontalCard />
                    <SubHeader title="최근에 열람한 목록" cssId="recently-list" />
                    <HorizontalCard /> <HorizontalCard /> <HorizontalCard /> <HorizontalCard />{' '}
                    <HorizontalCard />
                    <SubHeader title="구매한 목록" cssId="purchase-list" />
                    <HorizontalCard /> <HorizontalCard />
                </div>
            )}

            {key[0] === 'like' && (
                <Row gutter={16} style={{ margin: '10px 0' }}>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                    <Col span={12}>
                        <ClassCard classInfo={garaData} />
                    </Col>
                </Row>
            )}
        </div>
    )
}

const SubHeader = ({ title, cssId = 'default' }) => {
    return (
        <div className="mypage-sub-header" id={cssId}>
            {title}
        </div>
    )
}

export default mypage

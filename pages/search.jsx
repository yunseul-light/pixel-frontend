import { useState } from 'react'
import { Menu } from 'antd'
import SearchInput from '../src/components/common/SearchInput'
import TeacherCard from '../src/components/card/TeacherCard'
import PlanCard from '../src/components/card/PlanCard'
import NewClassesCard from '../src/components/card/NewClassesCard'

const menuItemStyles = {
  width: '25%',
  textAlign: 'center'
}

const menuStyles = {}

const search = () => {
  const [key, setKey] = useState(['popularity'])

  const handleClick = e => {
    console.log('click ', e)
    setKey([e.key])
  }

  return (
    <div>
      <SearchInput />
      <Menu
        mode='horizontal'
        style={menuStyles}
        defaultSelectedKeys={key}
        onClick={handleClick}
      >
        <Menu.Item key='popularity' style={menuItemStyles}>
          인기
        </Menu.Item>
        <Menu.Item key='tag' style={menuItemStyles}>
          태그
        </Menu.Item>
        <Menu.Item key='class' style={menuItemStyles}>
          강의
        </Menu.Item>
        <Menu.Item key='teacher' style={menuItemStyles}>
          강사
        </Menu.Item>
      </Menu>

      {key[0] === 'popularity' && (
        <div>
          <PlanCard />
          <PlanCard />
          <PlanCard />
          <PlanCard />
        </div>
      )}
      {key[0] === 'tag' && <div>tag</div>}
      {key[0] === 'class' && (
        <div>
          <NewClassesCard />
        </div>
      )}
      {key[0] === 'teacher' && (
        <div>
          <TeacherCard />
          <TeacherCard />
          <TeacherCard />
          <TeacherCard />
          <TeacherCard />
        </div>
      )}
    </div>
  )
}

export default search
